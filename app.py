from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/des', methods=['GET', 'POST'])
def des():
    res = []
    if request.method == "POST":
        # res = []
        txt1 = request.form.get('text')
        key = request.form.get('key')

        per1 = [57, 49, 41, 33, 25, 17, 9, 1,
                59, 51, 43, 35, 27, 19, 11, 3,
                61, 53, 45, 37, 29, 21, 13, 5,
                63, 55, 47, 39, 31, 23, 15, 7,
                56, 48, 40, 32, 24, 16, 8, 0,
                58, 50, 42, 34, 26, 18, 10, 2,
                60, 52, 44, 36, 28, 20, 12, 4,
                62, 54, 46, 38, 30, 22, 14, 6]

        per2 = [39, 7, 47, 15, 55, 23, 63, 31,
                38, 6, 46, 14, 54, 22, 62, 30,
                37, 5, 45, 13, 53, 21, 61, 29,
                36, 4, 44, 12, 52, 20, 60, 28,
                35, 3, 43, 11, 51, 19, 59, 27,
                34, 2, 42, 10, 50, 18, 58, 26,
                33, 1, 41, 9, 49, 17, 57, 25,
                32, 0, 40, 8, 48, 16, 56, 24, ]
        Kmatrix = (([
                        [14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
                        [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
                        [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
                        [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13]
                    ],
                    [
                        [15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],
                        [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
                        [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
                        [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9]
                    ],
                    [
                        [10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],
                        [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
                        [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
                        [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12]
                    ],
                    [
                        [7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15],
                        [13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9],
                        [10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4],
                        [3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14]
                    ],
                    [
                        [2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9],
                        [14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6],
                        [4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14],
                        [11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3]
                    ],
                    [
                        [12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11],
                        [10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8],
                        [9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6],
                        [4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13]
                    ],
                    [
                        [4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1],
                        [13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6],
                        [1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2],
                        [6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12]
                    ],
                    [
                        [13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7],
                        [1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2],
                        [7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8],
                        [2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11]
                    ]
        ))

        # Перевод текста в двоичную систему
        def hexbin(txt):
            v11 = ''
            for i1 in txt:
                z = ord(i1) - 848
                if i1 == ' ':
                    z = 32
                    v = bin(z)[2:]
                    v = str(v).zfill(8)
                else:
                    v = bin(z)[2:]
                    v = str(v)
                v11 += v
            return v11

        v1 = hexbin(txt1)
        res.append('Битовая последовательность до перестановки:   ' + v1)
        # Перестановка битов
        j2 = 0
        masp = ''
        for i2 in range(len(v1)):
            z1 = per1[j2]
            masp += v1[z1]
            j2 = j2 + 1
        res.append('Битовая последовательность после перестановки:' + masp)
        # Разделение битовой последовательности на блоки R и L
        L0 = masp[:32];
        R0 = masp[32:]
        res.append('Блок L: ' + L0)
        res.append('Блок R: ' + R0)
        # Разбиение блока R на 4-битовые подблоки и их расширение
        v4 = ''
        i3 = 0
        while i3 < len(R0):
            if i3 < len(R0) - 4 and i3 > 0:
                r1 = R0[i3:i3 + 4]
                v3 = r1 + R0[i3 + 4]
                v3 = R0[i3 - 1] + v3
                i3 = i3 + 4
            else:
                if i3 == 0:
                    r1 = R0[i3:i3 + 4]
                    v3 = r1 + R0[i3 + 4]
                    v3 = R0[len(R0) - 1] + v3
                    i3 = i3 + 4
                else:
                    r1 = R0[i3:i3 + 4]
                    v3 = R0[i3 - 1] + r1
                    v3 = v3 + R0[0]
                    i3 = i3 + 4
            v4 += v3
        res.append('Блок R с расширением подблоков до 6 битов:' + v4)
        # Сокращение ключа до 48 бит отбрасывая 7 и8 биты
        v5 = hexbin(key)
        i4 = 0
        key1 = ''
        while i4 < len(v5):
            k1 = v5[i4:i4 + 8]
            k2 = k1[:6]
            i4 = i4 + 8
            key1 += k2
        res.append('Ключ сокращенный до 48 бит: ' + key1)
        # Поразрядное суммирование сокращенного ключа и расширенного блока R
        i5 = 0
        xo1 = ''
        for i5 in range(len(v4)):
            x1 = int(v4[i5])
            x2 = int(key1[i5])
            xo = x1 ^ x2
            xo1 += str(xo)
        res.append('Строка после поразрядного суммирования блока R и ключа: ' + xo1)
        # Операция подстановки
        i6 = 0
        v6 = '';
        KK = 0
        while i6 < len(xo1):
            xo2 = xo1[i6 + 1:i6 + 5]
            xo3 = xo1[i6] + xo1[i6 + 5]
            i6 = i6 + 6
            index1 = int(xo3, base=2)
            index2 = int(xo2, base=2)
            v6 += bin(Kmatrix[KK][index1][index2])[2:].zfill(4)
            KK += 1
        res.append('Выходной блок R: ' + v6)
        # Соединение выходного блока R и L
        v7 = v6 + L0
        res.append('Соединяем блоки R и L: ' + v7)
        # Перестановка вида 2
        j3 = 0
        masp2 = ''
        for i8 in range(len(v7)):
            zz = per2[j3]
            masp2 += v7[zz]
            j3 = j3 + 1
        res.append('Итоговая шифрованая последовательность:' + masp2)

    return render_template('des.html', result=res)


@app.route('/gost', methods=['GET', 'POST'])
def gost():
    res = []
    if request.method == "POST":
        # res = []
        txt1 = request.form.get('text')
        key = request.form.get('key')

        Kmatrix = [
            [14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
            [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
            [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
            [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13]
        ]
        pre1 = [
            [1, 13, 4, 6, 7, 5, 14, 4],
            [15, 11, 11, 12, 13, 8, 11, 10],
            [13, 4, 10, 7, 10, 1, 4, 9],
            [0, 1, 0, 1, 1, 13, 12, 2],
            [5, 3, 7, 5, 0, 10, 6, 13],
            [7, 15, 2, 15, 8, 3, 13, 8],
            [10, 5, 1, 13, 9, 4, 15, 0],
            [4, 9, 13, 8, 15, 2, 10, 14],
            [9, 0, 3, 4, 14, 14, 2, 6],
            [2, 10, 6, 10, 4, 15, 3, 11],
            [3, 14, 8, 9, 6, 12, 8, 1],
            [14, 7, 5, 14, 12, 7, 1, 12],
            [6, 6, 9, 0, 11, 6, 0, 7],
            [11, 8, 12, 3, 2, 0, 7, 15],
            [8, 2, 15, 11, 5, 9, 5, 5],
            [12, 12, 14, 2, 3, 11, 9, 3]
        ];

        # Перевод текста в двоичную систему
        def hexbin(txt):
            v11 = ''
            for i1 in txt:
                z = ord(i1) - 848
                if i1 == ' ':
                    z = 32
                    v = bin(z)[2:]
                    v = str(v).zfill(8)
                else:
                    v = bin(z)[2:]
                    v = str(v)
                v11 += v
            return v11

        L0 = hexbin(txt1)[:32]
        R0 = hexbin(txt1)[32:]
        keybin = hexbin(key)
        res.append('Битовая последовательность L0: ' + L0)
        res.append('Битовая последовательность R0: ' + R0)
        res.append('Битовая последовательность X0: ' + keybin)
        # Вычисление суммы R0 и X0 по модулю 2 в степени 32
        R01 = (int(R0, base=2) + int(keybin, base=2)) % 2 ** 32;
        R01 = bin(R01)[2:]
        res.append('Результат суммирования R0 и X0 по mod(2^32):' + R01)
        # Преобразование в блоке подстановки
        i1 = 0;
        i2 = 0;
        block = ''
        while i1 < len(R01):
            xo1 = R01[i1:i1 + 4]
            i1 = i1 + 4
            index1 = int(xo1, base=2)
            block += bin(pre1[index1][i2])[2:].zfill(4)
            i2 = i2 + 1
        res.append('Преобразование в блоке подстановки: ' + block)
        # Циклический сдвиг на 11
        i3 = 0;
        sdv = ''
        for i3 in range(len(block)):
            sdv += block[i3 - 21]
            i3 = i3 + 1
        res.append('Результат циклического сдвига на 11:' + sdv)
        # Сложение по модулю 2 сдвинутой строки и блока L0
        i4 = 0;
        rez = ''
        for i4 in range(len(sdv)):
            x1 = int(L0[i4])
            x2 = int(sdv[i4])
            xo = x1 ^ x2
            rez += str(xo)
        res.append('Итоговая шифрованая последовательность: ' + R0 + rez)

    return render_template('gost.html', result=res)


@app.route('/hash', methods=['GET', 'POST'])
def hash():
    res = []

    res = []
    if request.method == "POST":
        # res = []
        txt1 = request.form.get('text')
        # key = request.form.get('key')
        txt2 = txt1
        # Проверка чисел на взимную простоту
        p = int(request.form.get('p'))
        q = int(request.form.get('q'))
        Hi = int(request.form.get('Hi'))
        n = p * q
        res.append('Входные данные: ')
        res.append('p ='+ str(p) +'q =' + str(q) +'n =' + str(n)+ 'H0 ='+ str(Hi))
        v21 = ''
        i3 = 0
        for i in txt2:
            M = ord(i) - 1039
            v21 += str(M) + ','
        res.append('Шифруемый текст в виде чисел ('+ v21 + ')')
        res.append('Нахождение хеш образа:')
        for i in txt2:
            i3 = i3 + 1
            M = ord(i) - 1039
            Hi = (Hi + M) ** 2 % n
            res.append("H" + str(i3) + '=' + str(Hi))
        res.append('Полученный хеш - образ сообщения:' + str(Hi))
    return render_template('hash.html',result=res)


@app.route('/rsa', methods=['GET', 'POST'])
def rsa():
    res = []
    if request.method == "POST":
        # res = []
        txt1 = request.form.get('text')
        key = request.form.get('key')

        res.append('1. Алгоритм RSA')
        # txt1 = input('Введите текст для шифрования: ')
        # Проверка чисел на взимную простоту
        p = int(request.form.get('p'))
        q = int(request.form.get('q'))
        n = p * q
        F1 = (p - 1) * (q - 1)
        res.append('Входные данные: ')
        res.append('p =' + str(p) + 'q =' + str(q) + 'n =' + str(n) + 'f(n) = ' + str(F1))

        def coprime(a, b):
            while a != 0 and b != 0:
                if a > b:
                    a %= b
                else:
                    b %= a
            gcd = a + b
            return gcd

        # Расчет числа d
        def dcalc(d, F):
            nod = 10
            while nod > 1:
                d = d + 1
                if nod > 1:
                    nod = coprime(d, F)
                else:
                    nod = coprime(d, F)
            return (d)

        # Расчет числа е в зависимости от d
        d2 = 2
        e = 10000
        while e > F1:
            k = 0
            e1 = 3
            d2 = dcalc(d2, F1)
            while e1 >= 1:
                k = k + 1
                e1 = (F1 * k + 1) % d2
                e = (F1 * k + 1) / d2

        res.append('Значение e =' + str(int(e)) + 'и значение d =' + str(d2) + 'удовлетворяющие условиям')
        res.append('Открытый ключ: (' + str(int(e)) + ',' + str(n) + ')')
        res.append('Закрытый ключ: (' + str(d2) + ',' + str(n) + ')')
        # Рассчет шифра
        v11 = ''
        v12 = ''
        v13 = ''
        i1 = 1
        for i in txt1:
            z = ord(i) - 1039
            z1 = z ** int(e) % n
            z2 = z1 ** d2 % n
            res.append("C" + str(i1) + '=' + str(z1))
            v11 += str(z) + ','
            v12 += str(z1) + ','
            v13 += str(z2) + ','
            i1 = i1 + 1
        res.append('Шифруемый текст в виде чисел (' + v11 + ')')
        res.append('Зашифрованный текст в виде чисел (' + v12 + ')')
        i1 = 1
        for i in txt1:
            z = ord(i) - 1039
            z1 = z ** int(e) % n
            z2 = z1 ** d2 % n
            res.append("M" + str(i1) + '=' + str(z2))
            i1 = i1 + 1
        res.append('Расшифрованный текст в виде чисел (' + v13 + ')')
    return render_template('rsa.html', result=res)


@app.route('/ecp', methods=['GET', 'POST'])
def ecp():
    res = []

    res = []
    if request.method == "POST":
        # res = []
        # Алгоритм ЭЦП
        
        txt2 = request.form.get('text')
        # Проверка чисел на взимную простоту
        h0 = int(request.form.get('h0'))
        p = int(request.form.get('p'))
        q = int(request.form.get('q'))
        n = p * q
        F2 = (p - 1) * (q - 1)
        res.append('Входные данные: ')
        res.append('p =' + str(p) + 'q =' + str(q)+  'h0 =' + str(h0) + 'n =' +str(n) +'f(n) = ' +str(F2))

        def coprime(a, b):
            while a != 0 and b != 0:
                if a > b:
                    a %= b
                else:
                    b %= a
            gcd = a + b
            return gcd

        # Расчет числа d
        def dcalc(d, F):
            nod = 10
            while nod > 1:
                d = d + 1
                if nod > 1:
                    nod = coprime(d, F)
                else:
                    nod = coprime(d, F)
            return (d)

        # Расчет числа е в зависимости от d
        d3 = 2
        e = 10000
        while e > F2:
            k1 = 0
            e2 = 3
            d3 = dcalc(d3, F2)
            while e2 >= 1:
                k1 = k1 + 1
                e2 = (F2 * k1 + 1) % d3
                e = (F2 * k1 + 1) / d3
        res.append('Значение e =' + str(int(e)) + 'и значение d =' + str(d3) + 'удовлетворяющие условиям')
        res.append('Открытый ключ: (' + str(int(e))+ ','+  str(n) +')')
        res.append('Закрытый ключ: ('+ str(d3)+ ','+ str(n) + ')')
        # Хеш - образ
        v21 = ''
        i3 = 0
        for i in txt2:
            M = ord(i) - 1039
            v21 += str(M) + ','
        res.append('Шифруемый текст в виде чисел ('+v21+')')

        res.append('Нахождение хеш образа:')
        # Поиск хеш - образа для эцп
        i3 = 0;
        Hi = 12
        for i in txt2:
            i3 = i3 + 1
            M1 = ord(i) - 1039
            Hi = (Hi + M1) ** 2 % n
            res.append("H" + str(i3)+ '='+ str(Hi))
        res.append('Полученный хеш - образ сообщения: r = '+ str(Hi))
        # Нахождение ЭЦП
        S = Hi ** d3 % n
        res.append('Найденная ЭЦП:'+ str(S))
        # Проверка ЭЦП на подлинность
        r1 = S ** int(e) % n
        res.append("Используем открытый ключ и найдем r'")
        res.append("r'=" + str(r1))
        if Hi == r1:
            res.append("Так как r' = r ="+ str(r1)+', ЭЦП является подлинной')
        else:
            res.append('ЭЦП  не является подлинной')
    return render_template('ecp.html', result=res)


if __name__ == '__main__':
    app.run(debug=True)
